
<?php
    /* note: chmod a+r this file to allow users to access it, and then move it into /var/www/html/ */



    /*
     *  generate a table with contents (autogen)
     *
     *
     */
    function genTables()
    {
    $maxcols = 8;
    $maxid = 16;
    $startid = 1;

    echo "<table id='table1'>\n";
    for ($i = 1;$i<=ceil($maxid/$maxcols);$i++)
    {
    echo "<tr>\n";
    for ($j=1;$j<=$maxcols;$j++)
        if ($startid <= $maxid)
            echo "  <td class='mark'>ID".$startid++."</td>\n";
        else
            echo "  <td> </td>\n";

        echo "</tr>\n<tr>\n";
        for ($j=1;$j<=$maxcols;$j++)
            echo "<td>Content</td>\n";

        echo "</tr>\n";
    }

    echo "</table>\n";

    }


    /*
     * generates a break
     *
     */
    function br()
    {
        echo "<br>";
    }


    /*
     * generate a h3 with content
     *
     */
    function h3($content)
    {
        echo "<h3>$content</h3>";
        br();
    }


    /*
     *  creates a span with content
     *
     */
    function span($content)
    {
        echo "<span> $content </span>";
    }

    /*
     *  add a hyperlink
     *
     */
    function hyperlink($url, $linktext)
    {
        echo "<a href=\"http://www.$url\"> $linktext </a>";
    }

    /*
     *  creates a button with a css class, onclick javascript function, and button text.
     *  works 6/18/2020
     */
    function button($classes, $id, $onclick, $text)
    {
        echo "<button id=\"$id\" class=\"$classes\" onclick=\"$onclick\">$text</button>";
    }




    ?>

<script>
    /*   place all javascript in here  */
    function text_change()
    {
        var b = document.getElementById("test").style.color;
        if(b == "black")
        {
            document.getElementById("test").style.color ="red";
        }
        else
        {
            document.getElementById("test").style.color = "black";
        }
    }
</script>