<?php
$libname = "dnmlib";

class Spec
{
    public $name = 'default spec';
    private $module = 'vnet';
    const API_URL = "http://10.0.0.63/";
    var $auth_key;
    var $auth_email;
    var $accessToken;

    public function __construct($email, $key)
    {
        $this->auth_key = $key;
        $this->auth_email = $email;
    }

    public function addToken($token)
    {
        $this->accessToken = $token;
    }
}



